// Module pattern
var gameBoard = (function(){
    let board = ['-', '-', '-', '-', '-', '-', '-', '-', '-'];
    const grid = document.querySelector('#game-board');
    const getGrid = () => grid;
    const getBoard = () => board;
    const updateBoard = (num, update) => board.splice(num, 1, update);
    const resetBoard = () => {
        board = ['-', '-', '-', '-', '-', '-', '-', '-', '-'];
    };

    const createGrid = (numOfRows, numOfColumns) => {
        grid.innerHTML = "";
        resetBoard();
        grid.style.setProperty('--grid-rows', numOfRows);
        grid.style.setProperty('--grid-columns', numOfColumns);

        let i = 0;
        while (i < numOfRows * numOfColumns) {
            const cell = document.createElement('div');
            cell.style.setProperty('width', '100%');
            cell.style.setProperty('height', '2rem');
            cell.innerHTML = board[i];
            grid.appendChild(cell).setAttribute('id', i);
            i++;
        }
    }

    return {
        createGrid,
        getGrid,
        getBoard,
        updateBoard,
        resetBoard
    }

}());

// factory function
const Player = (name, symbol) => {
    let playerName = name;
    const turn = true;
    let selection = [];

    const getName = () => playerName;
    const setName = (newName) => playerName = newName;
    const getSymbol = () => symbol;
    const getTurn = () => turn;
    const toggleTurn = () => !turn;
    const getSelection = () => selection;
    const updateSelection = (index) => selection.push(index);

    return { 
        getName,
        setName,
        getSymbol,
        getTurn,
        toggleTurn,
        getSelection,
        updateSelection
    }
}

// game module
var game = (function() {
    const getPlayerNames = (which) => {
        const playerOneName = document.querySelector('#player-one-name').value;
        const playerTwoName = document.querySelector('#player-two-name').value;
        
        return which == '1' ? playerOneName : playerTwoName;
    };

    const playerOne = Player('Player One', 'x');
    const playerTwo = Player('Player Two', 'o');

    const getPlayerOne = () => playerOne;
    const getPlayerTwo = () => playerTwo;

    let currentPlayer = 'x';

    const setPlayerIndicator = (player) => {
        const tag = document.querySelector('#current-player');
        tag.innerHTML = `Current Player: ${player.getName()}`;
    }

    const switchPlayer = () => {
        const switchPlayerButton = document.querySelector('#toggle-player');
        switchPlayerButton.addEventListener('click', (e) => {

            console.log({currentPlayer});
            if (currentPlayer == playerTwo.getSymbol()) {
                currentPlayer = playerOne.getSymbol();
                setPlayerIndicator(playerOne);
            } else {
                currentPlayer = playerTwo.getSymbol();
                setPlayerIndicator(playerTwo);

            }
            e.preventDefault();

        });

    };

    const getPlayerInput = (player) => {
        const cells = gameBoard.getGrid().querySelectorAll('div');

        cells.forEach((cell) => {
            cell.addEventListener('click', (e) => {
                cell.innerHTML = currentPlayer // player.getSymbol();
                
                player.updateSelection(cell.getAttribute('id'));
                gameBoard.updateBoard(cell.getAttribute('id'), currentPlayer);
                
                if (checkForWinner(player.getSymbol())) {
                    alert(`${player.getName()} Wins!`);
                    setupGame();
                }
                player.toggleTurn();
                console.log(player.getName());
            });
        
        })
    }

    const checkForWinner = (playerSymbol) => {
        const combos = [
            [0,1,2],
            [3,4,5],
            [6,7,8],
            [0,3,6],
            [1,4,7],
            [2,5,8],
            [2,4,6],
            [0,4,8]
        ]

        let check = false
        
        for (let i = 0; i < combos.length; i++){
            if (gameBoard.getBoard()[combos[i][0]] == playerSymbol && 
                gameBoard.getBoard()[combos[i][1]] == playerSymbol && 
                gameBoard.getBoard()[combos[i][2]] == playerSymbol) {
                    check = true;
                    break;                    
                } else {    
                    check = false;
                }
        }
        
        return check;
    }

    const update = (players) => {
        switchPlayer();
        players.forEach(player => {
            setPlayerIndicator(player);
            if (player.getTurn()) {
                console.log(`It's ${player.getName()}'s Turn`);
                
                getPlayerInput(player);
            }
        });
    }

    const setupGame = () => {
        gameBoard.createGrid(3,3);
        update([game.getPlayerOne(), game.getPlayerTwo()]);
    };

    return {
        setupGame,
        update,
        getPlayerOne,
        getPlayerTwo,
        setPlayerIndicator,
        getPlayerNames,
        switchPlayer
    }
  
}());

document.querySelector('#start-game').addEventListener('click', (e) => {
    game.getPlayerOne().setName(game.getPlayerNames('1'));
    game.getPlayerTwo().setName(game.getPlayerNames('2'));
    console.log(game.getPlayerNames('1'));
    
    game.setupGame();
    e.preventDefault();
});
